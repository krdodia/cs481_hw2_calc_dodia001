﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW2_Dodia001
{
    public partial class MainPage : ContentPage
    {
        int status = 1;
        double number1, number2;
        string mathOperator;

        public MainPage()
        { 
            InitializeComponent();
        }

        void OnPressNumber(object sender, EventArgs e) 
        {
            Button button = (Button)sender;
            String pressed = button.Text;

            if(this.resultNumber.Text == "0" || status < 0){
                this.resultNumber.Text = "";
                if (status < 0)
                    status *= -1;
            }

            this.resultNumber.Text += pressed;

            if (double.TryParse(this.resultNumber.Text, out double number))
            {
                if (status == 1)
                    number1 = number;
                else
                    number2 = number;
            }
        }

        void OnPressOperator(object sender, EventArgs e)
        {
                status = -2;
                Button button = (Button)sender;
                String pressed = button.Text;
                mathOperator = pressed;
        }

        void OnClear(object sender, EventArgs e)
        {
            number1 = 0;
            number2 = 0;
            status = 1;
            this.resultNumber.Text = "0";
        }
        void OnResult(object sender, EventArgs e){
             if(status == 2)
                {
                    var result = Calculate(number1, number2, mathOperator);
                    this.resultNumber.Text = Convert.ToString(result);
                    number1 = result;
                    status = -1;
                }
        }
        double Calculate(double n1, double n2, string op )
        {
            double result = 0;
                switch(op){
                    case "+" :
                    result = n1 + n2;
                    break;

                    case "-" :
                    result = n1 - n2;
                    break;

                    case "*":
                    result = n1 * n2;
                    break;

                    case "/":
                    result = n1 / n2;
                    break;
                }
                return result;
         }
    }
}
